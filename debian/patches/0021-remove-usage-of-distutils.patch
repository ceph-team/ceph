Description: Remove usage of distutils
 distutils is gone from Python 3.12, but there are replacements for all of
 its API. This patch fixes the situation, as Debian is moving toward the
 Python 3.12 interpreter.
Author: Thomas Goirand <zigo@debian.org>
Forwarded: no
Last-Update: 2024-04-24

Index: ceph/src/fmt/support/manage.py
===================================================================
--- ceph.orig/src/fmt/support/manage.py
+++ ceph/src/fmt/support/manage.py
@@ -14,7 +14,7 @@ from __future__ import print_function
 import datetime, docopt, errno, fileinput, json, os
 import re, requests, shutil, sys, tempfile
 from contextlib import contextmanager
-from distutils.version import LooseVersion
+from packaging.version import parse
 from subprocess import check_call
 
 
@@ -194,9 +194,9 @@ def update_site(env):
         if os.path.exists(html_dir):
             shutil.rmtree(html_dir)
         include_dir = env.fmt_repo.dir
-        if LooseVersion(version) >= LooseVersion('5.0.0'):
+        if parse(version) >= parse('5.0.0'):
             include_dir = os.path.join(include_dir, 'include', 'fmt')
-        elif LooseVersion(version) >= LooseVersion('3.0.0'):
+        elif parse(version) >= parse('3.0.0'):
             include_dir = os.path.join(include_dir, 'fmt')
         import build
         build.build_docs(version, doc_dir=target_doc_dir,
Index: ceph/src/pybind/cephfs/setup.py
===================================================================
--- ceph.orig/src/pybind/cephfs/setup.py
+++ ceph/src/pybind/cephfs/setup.py
@@ -11,10 +11,20 @@ if not pkgutil.find_loader('setuptools')
 else:
     from setuptools import setup
     from setuptools.extension import Extension
-from distutils.ccompiler import new_compiler
-from distutils.errors import CompileError, LinkError
+try:
+    from distutils.ccompiler import new_compiler
+except ImportError:
+    from setuptools._distutils.ccompiler import new_compiler
+try:
+    from distutils.errors import CompileError, LinkError
+except ImportError:
+    from setuptools._distutils.errors import CompileError, LinkError
+
 from itertools import filterfalse, takewhile
-import distutils.sysconfig
+try:
+    from distutils import sysconfig
+except ImportError:
+    import sysconfig
 
 
 def filter_unsupported_flags(compiler, flags):
@@ -46,8 +56,8 @@ def monkey_with_compiler(customize):
     return patched
 
 
-distutils.sysconfig.customize_compiler = \
-    monkey_with_compiler(distutils.sysconfig.customize_compiler)
+sysconfig.customize_compiler = \
+    monkey_with_compiler(sysconfig.customize_compiler)
 
 # PEP 440 versioning of the Ceph FS package on PyPI
 # Bump this version, after every changeset
@@ -57,20 +67,20 @@ __version__ = '2.0.0'
 
 def get_python_flags(libs):
     py_libs = sum((libs.split() for libs in
-                   distutils.sysconfig.get_config_vars('LIBS', 'SYSLIBS')), [])
+                   sysconfig.get_config_vars('LIBS', 'SYSLIBS')), [])
     ldflags = list(filterfalse(lambda lib: lib.startswith('-l'), py_libs))
     py_libs = [lib.replace('-l', '') for lib in
                filter(lambda lib: lib.startswith('-l'), py_libs)]
     compiler = new_compiler()
-    distutils.sysconfig.customize_compiler(compiler)
+    sysconfig.customize_compiler(compiler)
     return dict(
-        include_dirs=[distutils.sysconfig.get_python_inc()],
-        library_dirs=distutils.sysconfig.get_config_vars('LIBDIR', 'LIBPL'),
+        include_dirs=[sysconfig.get_python_inc()],
+        library_dirs=sysconfig.get_config_vars('LIBDIR', 'LIBPL'),
         libraries=libs + py_libs,
         extra_compile_args=filter_unsupported_flags(
             compiler.compiler[0],
-            compiler.compiler[1:] + distutils.sysconfig.get_config_var('CFLAGS').split()),
-        extra_link_args=(distutils.sysconfig.get_config_var('LDFLAGS').split() +
+            compiler.compiler[1:] + sysconfig.get_config_var('CFLAGS').split()),
+        extra_link_args=(sysconfig.get_config_var('LDFLAGS').split() +
                          ldflags))
 
 
@@ -101,7 +111,7 @@ def check_sanity():
         fp.write(dummy_prog)
 
     compiler = new_compiler()
-    distutils.sysconfig.customize_compiler(compiler)
+    sysconfig.customize_compiler(compiler)
 
     if 'CEPH_LIBDIR' in os.environ:
         # The setup.py has been invoked by a top-level Ceph make.
Index: ceph/src/pybind/mgr/dashboard/tools.py
===================================================================
--- ceph.orig/src/pybind/mgr/dashboard/tools.py
+++ ceph/src/pybind/mgr/dashboard/tools.py
@@ -9,7 +9,6 @@ import threading
 import time
 import urllib
 from datetime import datetime, timedelta
-from distutils.util import strtobool
 
 import cherrypy
 from mgr_util import build_url
@@ -26,6 +25,22 @@ except ImportError:
     pass  # For typing only
 
 
+def _strtobool(val):
+    """Convert a string representation of truth to true (1) or false (0).
+
+    True values are 'y', 'yes', 't', 'true', 'on', and '1'; false values
+    are 'n', 'no', 'f', 'false', 'off', and '0'.  Raises ValueError if
+    'val' is anything else.
+    """
+    # Copied from distutils
+    val = val.lower()
+    if val in ('y', 'yes', 't', 'true', 'on', '1'):
+        return 1
+    elif val in ('n', 'no', 'f', 'false', 'off', '0'):
+        return 0
+    else:
+        raise ValueError("invalid truth value %r" % (val,))
+
 class RequestLoggingTool(cherrypy.Tool):
     def __init__(self):
         cherrypy.Tool.__init__(self, 'before_handler', self.request_begin,
@@ -745,7 +760,7 @@ def str_to_bool(val):
     """
     if isinstance(val, bool):
         return val
-    return bool(strtobool(val))
+    return bool(_strtobool(val))
 
 
 def json_str_to_object(value):  # type: (AnyStr) -> Any
Index: ceph/src/pybind/mgr/volumes/fs/operations/pin_util.py
===================================================================
--- ceph.orig/src/pybind/mgr/volumes/fs/operations/pin_util.py
+++ ceph/src/pybind/mgr/volumes/fs/operations/pin_util.py
@@ -3,11 +3,28 @@ import errno
 import cephfs
 
 from ..exception import VolumeException
-from distutils.util import strtobool
+
+
+def _strtobool(val):
+    """Convert a string representation of truth to true (1) or false (0).
+
+    True values are 'y', 'yes', 't', 'true', 'on', and '1'; false values
+    are 'n', 'no', 'f', 'false', 'off', and '0'.  Raises ValueError if
+    'val' is anything else.
+    """
+    # Copied from distutils
+    val = val.lower()
+    if val in ('y', 'yes', 't', 'true', 'on', '1'):
+        return 1
+    elif val in ('n', 'no', 'f', 'false', 'off', '0'):
+        return 0
+    else:
+        raise ValueError("invalid truth value %r" % (val,))
+
 
 _pin_value = {
     "export": lambda x: int(x),
-    "distributed": lambda x: int(strtobool(x)),
+    "distributed": lambda x: int(_strtobool(x)),
     "random": lambda x: float(x),
 }
 _pin_xattr = {
Index: ceph/src/pybind/rados/setup.py
===================================================================
--- ceph.orig/src/pybind/rados/setup.py
+++ ceph/src/pybind/rados/setup.py
@@ -5,9 +5,18 @@ if not pkgutil.find_loader('setuptools')
 else:
     from setuptools import setup
     from setuptools.extension import Extension
-import distutils.sysconfig
-from distutils.errors import CompileError, LinkError
-from distutils.ccompiler import new_compiler
+try:
+    from distutils import sysconfig
+except ImportError:
+    import sysconfig
+try:
+    from distutils.errors import CompileError, LinkError
+except ImportError:
+    from setuptools._distutils.errors import CompileError, LinkError
+try:
+    from distutils.ccompiler import new_compiler
+except ImportError:
+    from setuptools._distutils.ccompiler import new_compiler
 from itertools import filterfalse, takewhile
 
 import os
@@ -46,8 +55,8 @@ def monkey_with_compiler(customize):
     return patched
 
 
-distutils.sysconfig.customize_compiler = \
-    monkey_with_compiler(distutils.sysconfig.customize_compiler)
+sysconfig.customize_compiler = \
+    monkey_with_compiler(sysconfig.customize_compiler)
 
 # PEP 440 versioning of the Rados package on PyPI
 # Bump this version, after every changeset
@@ -56,20 +65,20 @@ __version__ = '2.0.0'
 
 def get_python_flags(libs):
     py_libs = sum((libs.split() for libs in
-                   distutils.sysconfig.get_config_vars('LIBS', 'SYSLIBS')), [])
+                   sysconfig.get_config_vars('LIBS', 'SYSLIBS')), [])
     ldflags = list(filterfalse(lambda lib: lib.startswith('-l'), py_libs))
     py_libs = [lib.replace('-l', '') for lib in
                filter(lambda lib: lib.startswith('-l'), py_libs)]
     compiler = new_compiler()
-    distutils.sysconfig.customize_compiler(compiler)
+    sysconfig.customize_compiler(compiler)
     return dict(
-        include_dirs=[distutils.sysconfig.get_python_inc()],
-        library_dirs=distutils.sysconfig.get_config_vars('LIBDIR', 'LIBPL'),
+        include_dirs=[sysconfig.get_python_inc()],
+        library_dirs=sysconfig.get_config_vars('LIBDIR', 'LIBPL'),
         libraries=libs + py_libs,
         extra_compile_args=filter_unsupported_flags(
             compiler.compiler[0],
-            compiler.compiler[1:] + distutils.sysconfig.get_config_var('CFLAGS').split()),
-        extra_link_args=(distutils.sysconfig.get_config_var('LDFLAGS').split() +
+            compiler.compiler[1:] + sysconfig.get_config_var('CFLAGS').split()),
+        extra_link_args=(sysconfig.get_config_var('LDFLAGS').split() +
                          ldflags))
 
 
@@ -99,7 +108,7 @@ def check_sanity():
         fp.write(dummy_prog)
 
     compiler = new_compiler()
-    distutils.sysconfig.customize_compiler(compiler)
+    sysconfig.customize_compiler(compiler)
 
     if 'CEPH_LIBDIR' in os.environ:
         # The setup.py has been invoked by a top-level Ceph make.
Index: ceph/src/pybind/rbd/setup.py
===================================================================
--- ceph.orig/src/pybind/rbd/setup.py
+++ ceph/src/pybind/rbd/setup.py
@@ -11,11 +11,20 @@ if not pkgutil.find_loader('setuptools')
 else:
     from setuptools import setup
     from setuptools.extension import Extension
-from distutils.ccompiler import new_compiler
-from distutils.errors import CompileError, LinkError
+try:
+    from distutils.ccompiler import new_compiler
+except ImportError:
+    from setuptools._distutils.ccompiler import new_compiler
+try:
+    from distutils.errors import CompileError, LinkError
+except ImportError:
+    from setuptools._distutils.errors import CompileError, LinkError
 from itertools import filterfalse, takewhile
 from packaging import version
-import distutils.sysconfig
+try:
+    from distutils import sysconfig
+except ImportError:
+    import sysconfig
 
 
 def filter_unsupported_flags(compiler, flags):
@@ -47,8 +56,8 @@ def monkey_with_compiler(customize):
     return patched
 
 
-distutils.sysconfig.customize_compiler = \
-    monkey_with_compiler(distutils.sysconfig.customize_compiler)
+sysconfig.customize_compiler = \
+    monkey_with_compiler(sysconfig.customize_compiler)
 
 # PEP 440 versioning of the RBD package on PyPI
 # Bump this version, after every changeset
@@ -58,20 +67,20 @@ __version__ = '2.0.0'
 
 def get_python_flags(libs):
     py_libs = sum((libs.split() for libs in
-                   distutils.sysconfig.get_config_vars('LIBS', 'SYSLIBS')), [])
+                   sysconfig.get_config_vars('LIBS', 'SYSLIBS')), [])
     ldflags = list(filterfalse(lambda lib: lib.startswith('-l'), py_libs))
     py_libs = [lib.replace('-l', '') for lib in
                filter(lambda lib: lib.startswith('-l'), py_libs)]
     compiler = new_compiler()
-    distutils.sysconfig.customize_compiler(compiler)
+    sysconfig.customize_compiler(compiler)
     return dict(
-        include_dirs=[distutils.sysconfig.get_python_inc()],
-        library_dirs=distutils.sysconfig.get_config_vars('LIBDIR', 'LIBPL'),
+        include_dirs=[sysconfig.get_python_inc()],
+        library_dirs=sysconfig.get_config_vars('LIBDIR', 'LIBPL'),
         libraries=libs + py_libs,
         extra_compile_args=filter_unsupported_flags(
             compiler.compiler[0],
-            compiler.compiler[1:] + distutils.sysconfig.get_config_var('CFLAGS').split()),
-        extra_link_args=(distutils.sysconfig.get_config_var('LDFLAGS').split() +
+            compiler.compiler[1:] + sysconfig.get_config_var('CFLAGS').split()),
+        extra_link_args=(sysconfig.get_config_var('LDFLAGS').split() +
                          ldflags))
 
 
@@ -101,7 +110,7 @@ def check_sanity():
         fp.write(dummy_prog)
 
     compiler = new_compiler()
-    distutils.sysconfig.customize_compiler(compiler)
+    sysconfig.customize_compiler(compiler)
 
     if 'CEPH_LIBDIR' in os.environ:
         # The setup.py has been invoked by a top-level Ceph make.
Index: ceph/src/pybind/rgw/setup.py
===================================================================
--- ceph.orig/src/pybind/rgw/setup.py
+++ ceph/src/pybind/rgw/setup.py
@@ -5,17 +5,25 @@ if not pkgutil.find_loader('setuptools')
 else:
     from setuptools import setup
     from setuptools.extension import Extension
-import distutils.core
 
 import os
 import shutil
 import sys
 import tempfile
 import textwrap
-from distutils.ccompiler import new_compiler
-from distutils.errors import CompileError, LinkError
+try:
+    from distutils.ccompiler import new_compiler
+except ImportError:
+    from setuptools._distutils.ccompiler import new_compiler
+try:
+    from distutils.errors import CompileError, LinkError
+except ImportError:
+    from setuptools._distutils.errors import CompileError, LinkError
 from itertools import filterfalse, takewhile
-import distutils.sysconfig
+try:
+    from distutils import sysconfig
+except ImportError:
+    import sysconfig
 
 
 def filter_unsupported_flags(compiler, flags):
@@ -47,8 +55,8 @@ def monkey_with_compiler(customize):
     return patched
 
 
-distutils.sysconfig.customize_compiler = \
-    monkey_with_compiler(distutils.sysconfig.customize_compiler)
+sysconfig.customize_compiler = \
+    monkey_with_compiler(sysconfig.customize_compiler)
 
 # PEP 440 versioning of the RGW package on PyPI
 # Bump this version, after every changeset
@@ -58,20 +66,20 @@ __version__ = '2.0.0'
 
 def get_python_flags(libs):
     py_libs = sum((libs.split() for libs in
-                   distutils.sysconfig.get_config_vars('LIBS', 'SYSLIBS')), [])
+                   sysconfig.get_config_vars('LIBS', 'SYSLIBS')), [])
     ldflags = list(filterfalse(lambda lib: lib.startswith('-l'), py_libs))
     py_libs = [lib.replace('-l', '') for lib in
                filter(lambda lib: lib.startswith('-l'), py_libs)]
     compiler = new_compiler()
-    distutils.sysconfig.customize_compiler(compiler)
+    sysconfig.customize_compiler(compiler)
     return dict(
-        include_dirs=[distutils.sysconfig.get_python_inc()],
-        library_dirs=distutils.sysconfig.get_config_vars('LIBDIR', 'LIBPL'),
+        include_dirs=[sysconfig.get_python_inc()],
+        library_dirs=sysconfig.get_config_vars('LIBDIR', 'LIBPL'),
         libraries=libs + py_libs,
         extra_compile_args=filter_unsupported_flags(
             compiler.compiler[0],
-            compiler.compiler[1:] + distutils.sysconfig.get_config_var('CFLAGS').split()),
-        extra_link_args=(distutils.sysconfig.get_config_var('LDFLAGS').split() +
+            compiler.compiler[1:] + sysconfig.get_config_var('CFLAGS').split()),
+        extra_link_args=(sysconfig.get_config_var('LDFLAGS').split() +
                          ldflags))
 
 
@@ -101,7 +109,7 @@ def check_sanity():
         fp.write(dummy_prog)
 
     compiler = new_compiler()
-    distutils.sysconfig.customize_compiler(compiler)
+    sysconfig.customize_compiler(compiler)
 
     if 'CEPH_LIBDIR' in os.environ:
         # The setup.py has been invoked by a top-level Ceph make.
Index: ceph/src/seastar/dpdk/doc/guides/conf.py
===================================================================
--- ceph.orig/src/seastar/dpdk/doc/guides/conf.py
+++ ceph/src/seastar/dpdk/doc/guides/conf.py
@@ -4,7 +4,7 @@
 from __future__ import print_function
 import subprocess
 from docutils import nodes
-from distutils.version import LooseVersion
+from packaging.version import parse
 from sphinx import __version__ as sphinx_version
 from sphinx.highlighting import PygmentsBridge
 from pygments.formatters.latex import LatexFormatter
@@ -402,7 +402,7 @@ def setup(app):
                             'Features availability in compression drivers',
                             'Feature')
 
-    if LooseVersion(sphinx_version) < LooseVersion('1.3.1'):
+    if parse(sphinx_version) < parse('1.3.1'):
         print('Upgrade sphinx to version >= 1.3.1 for '
               'improved Figure/Table number handling.')
         # Add a role to handle :numref: references.
Index: ceph/src/seastar/scripts/perftune.py
===================================================================
--- ceph.orig/src/seastar/scripts/perftune.py
+++ ceph/src/seastar/scripts/perftune.py
@@ -2,7 +2,6 @@
 
 import abc
 import argparse
-import distutils.util
 import enum
 import functools
 import glob
@@ -22,6 +21,22 @@ import yaml
 import platform
 import shlex
 
+def _strtobool(val):
+    """Convert a string representation of truth to true (1) or false (0).
+
+    True values are 'y', 'yes', 't', 'true', 'on', and '1'; false values
+    are 'n', 'no', 'f', 'false', 'off', and '0'.  Raises ValueError if
+    'val' is anything else.
+    """
+    # Copied from distutils
+    val = val.lower()
+    if val in ('y', 'yes', 't', 'true', 'on', '1'):
+        return 1
+    elif val in ('n', 'no', 'f', 'false', 'off', '0'):
+        return 0
+    else:
+        raise ValueError("invalid truth value %r" % (val,))
+
 dry_run_mode = False
 def perftune_print(log_msg, *args, **kwargs):
     if dry_run_mode:
@@ -1585,7 +1600,7 @@ def extend_and_unique(orig_list, iterabl
 def parse_tri_state_arg(value, arg_name):
     try:
         if value is not None:
-            return distutils.util.strtobool(value)
+            return _strtobool(value)
         else:
             return None
     except:
@@ -1635,10 +1650,10 @@ def parse_options_file(prog_args):
         prog_args.devs = extend_and_unique(prog_args.devs, y['dev'])
 
     if 'write_back_cache' in y:
-        prog_args.set_write_back = distutils.util.strtobool("{}".format(y['write_back_cache']))
+        prog_args.set_write_back = _strtobool("{}".format(y['write_back_cache']))
 
     if 'arfs' in y:
-        prog_args.enable_arfs = distutils.util.strtobool("{}".format(y['arfs']))
+        prog_args.enable_arfs = _strtobool("{}".format(y['arfs']))
 
     if 'num_rx_queues' in y:
         prog_args.num_rx_queues = int(y['num_rx_queues'])
Index: ceph/src/spdk/dpdk/doc/guides/conf.py
===================================================================
--- ceph.orig/src/spdk/dpdk/doc/guides/conf.py
+++ ceph/src/spdk/dpdk/doc/guides/conf.py
@@ -4,7 +4,7 @@
 from __future__ import print_function
 import subprocess
 from docutils import nodes
-from distutils.version import LooseVersion
+from packaging.version import parse
 from sphinx import __version__ as sphinx_version
 from sphinx.highlighting import PygmentsBridge
 from pygments.formatters.latex import LatexFormatter
@@ -420,7 +420,7 @@ def setup(app):
                             'Features availability in bbdev drivers',
                             'Feature')
 
-    if LooseVersion(sphinx_version) < LooseVersion('1.3.1'):
+    if parse(sphinx_version) < parse('1.3.1'):
         print('Upgrade sphinx to version >= 1.3.1 for '
               'improved Figure/Table number handling.')
         # Add a role to handle :numref: references.
Index: ceph/src/test/rgw/bucket_notification/test_bn.py
===================================================================
--- ceph.orig/src/test/rgw/bucket_notification/test_bn.py
+++ ceph/src/test/rgw/bucket_notification/test_bn.py
@@ -2134,14 +2134,28 @@ def test_ps_s3_creation_triggers_on_mast
     ps_s3_creation_triggers_on_master(external_endpoint_address="amqp://localhost:5672")
 
 
+def _strtobool(val):
+    """Convert a string representation of truth to true (1) or false (0).
+
+    True values are 'y', 'yes', 't', 'true', 'on', and '1'; false values
+    are 'n', 'no', 'f', 'false', 'off', and '0'.  Raises ValueError if
+    'val' is anything else.
+    """
+    # Copied from distutils
+    val = val.lower()
+    if val in ('y', 'yes', 't', 'true', 'on', '1'):
+        return 1
+    elif val in ('n', 'no', 'f', 'false', 'off', '0'):
+        return 0
+    else:
+        raise ValueError("invalid truth value %r" % (val,))
+
 @attr('amqp_ssl_test')
 def test_ps_s3_creation_triggers_on_master_external():
 
-    from distutils.util import strtobool
-
     if 'AMQP_EXTERNAL_ENDPOINT' in os.environ:
         try:
-            if strtobool(os.environ['AMQP_VERIFY_SSL']):
+            if _strtobool(os.environ['AMQP_VERIFY_SSL']):
                 verify_ssl = 'true'
             else:
                 verify_ssl = 'false'
